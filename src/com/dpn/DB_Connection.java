package com.dpn;
import java.sql.Connection;
import java.sql.DriverManager;

public class DB_Connection {

	public DB_Connection() {
		// TODO Auto-generated constructor stub
	}
	public Connection get_connection()
	{
		Connection connection=null;
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/phonebook","root","");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return connection;
		
	}

	public static void main(String[] args) {
		
		DB_Connection conn_obj=new DB_Connection();
		System.out.println(conn_obj.get_connection());
		
	}

}
