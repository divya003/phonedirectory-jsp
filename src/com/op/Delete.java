package com.op;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.dpn.DB_Connection;
import com.dpn.Contact_Bean;
public class Delete {

	public Delete() {
		// TODO Auto-generated constructor stub
	}
	public void delete_values(String id)
	{
		DB_Connection obj=new DB_Connection();
		Connection connection=obj.get_connection();
		PreparedStatement ps=null;
		
		try
		{
			String query="DELETE FROM details WHERE id=?";
			
			ps=connection.prepareStatement(query);
			ps.setString(1,id);
			ps.executeUpdate();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}


}
