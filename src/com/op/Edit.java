package com.op;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.dpn.DB_Connection;
import com.dpn.Contact_Bean;


public class Edit {

	public Edit() {
		// TODO Auto-generated constructor stub
	}
	public Contact_Bean get_edit_values(String id)
	{
		DB_Connection obj=new DB_Connection();
		Connection connection=obj.get_connection();
		PreparedStatement ps=null;
		ResultSet rs=null;
		Contact_Bean beanobj=new Contact_Bean();
		try
		{
			String query="SELECT * FROM details WHERE id=?";
			
			ps=connection.prepareStatement(query);
			ps.setString(1,id);
			rs=ps.executeQuery();
			while(rs.next())
			{
				beanobj.setid(rs.getInt("id"));
				beanobj.setName(rs.getString("name"));
				beanobj.setPhone(rs.getString("phone"));
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return beanobj;
	}
	

	public void edit_values(Contact_Bean beanobj)
	{
		DB_Connection obj=new DB_Connection();
		Connection connection=obj.get_connection();
		PreparedStatement ps=null;
		
		try
		{
			String query="UPDATE details SET name=?,phone=? WHERE id=?";
			
			ps=connection.prepareStatement(query);
			//ps.setString(1,id );
			//ps.setString(3,name );
			//ps.setString(2,phone );
			ps.setString(3,Integer.toString(beanobj.getid()));
			ps.setString(1,beanobj.getName());
			ps.setString(2,beanobj.getPhone());
			ps.executeUpdate();
			
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}

}
